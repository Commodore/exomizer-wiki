# Exomizer 3 website
Exomizer is a program that compresses files in a way that tries to be as efficient as possible but still allows them to be decompressed in environments where CPU speed and RAM are limited. For some popular 8-bit computers using 6502 compatible CPUs it can also generate executable files that decompress themselves in memory when run.

The exomizer program itself is a console application written in ANSI-C.

Exomizer operates in two major modes. It has a raw mode that compresses plain files and produces plain files. This mode is used by the raw subcommand. All other subcommands use the second mode. It requires all source files to be targeted to be decrunched at specific addresses in the 16-bit address space of the target computer. In this mode, no file can be larger than 64kB since all data must be addressable using 16-bits. The targeted mode has the following features:

* It reads plain or .prg files to any given address.
* It can produce stand-alone self-decrunching files for the following targets:
    * Commodore VIC20, C64, C65, C16/plus4, C128 and PET 4032
    * Atari 400/800 XL/XE
    * Apple ][+ and //e
    * Oric-1 and Oric Atmos
    * BBC Micro B
* It can produce files for both "in memory" and "from disk" decrunching.
* It handles RLE-sequences well, no packer is necessary.
* It is able to link/combine more than one source file into the same crunched target file.

Included in the downloadable zip file are the source code and a precompiled binary for Win32. It also includes a makefile for Gnu make and gcc so it should be easy to build on any system where these tools are available.

Any suggestions, comments and/or bug reports can be sent to [me](mailto:magli143@gmail.com), the author.

### 2023-11-25: Exomizer v3.1.2 released
Changes since the 3.1.1 release:

* Change to zlib license for all files not contributed by others.
* The logic in the sfx decruncher should also check if the setup copy area accesses RAM below ROM when deciding ram_during. Bug reported by Comos.
* The desfx sub command should only consider mem areas created after setup for detection of the decrunched data. Bug reported by Gi-Joe.
* Make c65 sfx target disable IRQ:s on exit in more situations.
* Fix Atari 8bit sfx zero page addr collision.
* Improve zp-usage and fix sfx basic for target PET 4032.
* The exodecr/exodecrunch.s 6502 decruncher now also exists in a version for kick assembler.

A complete changelog is included in the zip file. Download it [here](downloads/exomizer-3.1.2.zip).

### 2021-05-22: Exomizer v3.1.1 released
Major changes since the 3.1.0 release:

* New sfx target, -t 65, for the Commodore c65 and the MEGA65 computer.
* New -y option to the sfx command to add text to the end of the generated SYS-line.

(download removed)

### 2020-12-22: Exomizer v3.1.0 released
Changes since the 3.0.2 release:

* Improved compression on average due to offset reuse.
* Add experimental split encoding feature for encoding reuse which is a gain when compressing similar small files (only supported by 6502 decruncher).
* New Intel 8080 decompressor by Ivan Gorodetsky
* New ARM 32bit thumb2 decompressor by ALeX Kazik
* Updated Z80 decompressor by Antonio Villena.
* Miscellaneous bug fixes.

(download removed)

### 2019-01-05: Exomizer v3.0.2 released
Changes since the 3.0.1 release:

* Add documentation about level, mem and raw outfile structure.
* Fix raw -d -r combination to reverse the inbuffer before decrunch.
* Fix -P0 literal sequences bug in exodec.c, Bug reported by Nino Porcino.
* improved cruncher tuning for slightly better compression on average.
* Added sfx support for a new target, BBC Micro B (-t 0xbbcb).
* Added -P+16 awareness to exodecrunch.s
* Fix bucket optimization to be -T+4 aware in search.c, Bug eported by Ciccioriccio.
* Fix absolute offset overflow bug in sfxdecr.s. Bug reported by Comos.

A complete changelog is included in the zip file. Download it [here](downloads/exomizer-3.0.2.zip).

### 2018-08-10: Exomizer v3.0.1 released
Changes since the 3.0.0 release:

* Added missing clc to 6502 decrunchers (sfx and exodecrunch.s). This made decrunch fail if the decruncher was entered with the carry flag set. Bug reported by soci.

(download removed)

### 2018-05-16: Exomizer v3.0.0 released
Changes since the 2.0.x releases:

* Faster 6502 decrunchers (sfx and exodecrunch.s). Up to almost 50% faster than 2.0.11 if you are willing to trade some size for speed. To facilitate this the default bitstream format has changed. See exo30info.txt for details and benchmarks.
* Very slightly improved compression for some files.

(download removed)

### 2018-03-08: Exomizer v2.0.11 released
Changes since the 2.0.10 release:

* Now supports the Applesingle file format for reading. It is also written by sfx when the Apple ][+ target is selected. The Applesingle format is also supported by cc65 2.17 and AppleCommander 1.4.0 beta. This change was suggested by Oliver Schmidt.
* The sfx sub command recognises a new string "bin" as a shortcut for setting i_load_addr and run addr to the load addr of the in file.
* The sfx sub command is now for some situations able to autodetect the target for sfx bin and sfx <addr> by looking at the format of the in-files. The auto detection will succeed if the formats are non conflicting and one of xex, Oric tap or Applesingle (optionally combined with raw files).
* Fixed hard to trigger compression iteration crashing bug caused by the thread safety changes for library compilation introduced in 2.0.10, reported by Lasse Öörni.

A complete changelog is included in the zip file. Download it [here](downloads/exomizer-2.0.11.zip).

### 2017-12-25: Exomizer v2.0.10 released
Changes since the 2.0.9 release:

* No more precompiled binaries for DOS.
* Improved compression slightly for some files. However also making it slightly slower. Use -C flag to disable.
* sfx decruncher for +4/c16 now doesn't overwrite current device number.
* Add new sfx target, PET 4032, from the nanoflite github fork.
* It is now possible to set offset and length to plain and prg file loading.
* New less talkative output mode. Enabled by -B flag.
* Internal changes to make exomizer more thread safe. (for library usage)
* Show progress only when stdout is a TTY and not when it is redirected.
* Include more details of the memory layout in the output of the sfx sub command.
* Add license information to the contributed z80 decrunchers.
* Improve portability by tweaking Makefiles and not using negative exit codes.
* Exit with error if the parsing of sfxdecr fails.
* New sfx keyword systrim. Behaves like sys but also removes the sys-line from the crunched area.

A complete changelog is included in the zip file. (download removed)

### 2015-09-21: Exomizer v2.0.9 released
Changes since the 2.0.8 release:

* NULL pointer dereference crash, reported by Flavio, fix provided by soci.
* sfx decr src comments echoed to stdout, reported by iAN CooG, fix provided by soci.
* Fix gcc-compiler warnings.

A complete changelog is included in the zip file. (download removed)

### 2015-09-20: Exomizer v2.0.8 released
Changes since the 2.0.7 release:

* Add -E flag to not write the encoding to the outfile.
* Enforce match max_len everwhere, bug first reported by Zik / Futurs.
* Remove max nr of chunks limit from the chunkpool allocator to allow for raw crunching of larger files.
* Make exomizer build with bison version 2.3a and newer. Bug reported and analyzed by Adrien Destugues.

A complete changelog is included in the zip file. (download removed)

### 2013-04-14: Exomizer v2.0.7 released
Changes since the 2.0.6 release:

* Bugfixed commodore sfx targets to automatically disable irq when decrunching over system areas. This was reported for the plus4 target by Luca/FIRE. This fix together with moving the decrunch table to zero-page, -Di_table_addr=0x2, allows decrunching $0200-<end of mem> without corruption for all commodore targets except for the vic20-configs that have a memory hole at $0400-$1000.
* Bugfixed z80 decrunchers from Metalbrain.
* Bugfixed sfx c16/plus4 target where the default decrunch effect could corrupt memory while decrunching data that covers $0be7, reported by Luca/FIRE.
* Added feature to sfx-mode that complains if the data it too big to fit in the available memory of the selected target, suggested by Luca/FIRE.
* Added c16 target, -t 16, like -t4 but with smaller memory, suggested by Luca/FIRE.

A complete changelog is included in the zip file. (download removed)

### 2013-01-27: Exomizer v2.0.6 released
Changes since the 2.0.5 release:

* Improvements to the z80 decunchers, now smaller and faster.

A complete changelog is included in the zip file. (download removed)

### 2013-01-12: Exomizer v2.0.5 released
Changes since the 2.0.4 release:

* Added two flags -C, and -M <length>, that trades crunch result for speed. -C enables more imprecise rle matching and -M limits the maximum sequence length. It is now possible to really speed up crunching, even for "worst case"-type files.
* Now skips the DAG traversing of the final pass if the encoding hasn't changed since the previous pass.

A complete changelog is included in the zip file. (download removed)

### 2012-08-16: Exomizer v2.0.4 released
Changes since the 2.0.3 release:

* Bug fixed in the z80 decruncher.
* Enabled complete sfx basic mode support for the Apple II target. This mode and target combination now generates headerless files for easier import into disk images. The included exo20info.txt file contains more details about this.

A complete changelog is included in the zip file. (download removed)

### 2012-03-25: Exomizer v2.0.3 released
Changes since the 2.0.2 release:

* z80 decruncher improvements by Antonio Villena.

A complete changelog is included in the zip file. (download removed)

### 2011-08-19: Exomizer v2.0.2 released
Changes since the 2.0.1 release:

* Added 6809-decruncher contributed by Edouard Forler.
* Fix language errors in the documentation. Thanks to Csabo/LOD.
* Remove bogus printout about the default decrunch effect when using a custom decrunch effect. Bug reported by Csabo/LOD.
* Fix bug that prevented the correct error message from showing when trying to combine a basic start and a non rom config for the sfx command. Bug reported by iAN CooG.

A complete changelog is included in the zip file. (download removed)

### 2011-02-06: Exomizer v2.0.1 released
Changes since the 2.0 release:

* Fixed a bug in the logging framework that caused the desfx command to loop.

A complete changelog is included in the zip file. (download removed)

### 2011-01-22: Exomizer v2.0 released
Changes since the 2.0 beta 7 release:
* Fixed a bug in sfx decruncher setup where data was copied unnecessary.
* Added the i_line_number symbol to be able to change the default line number of the basic line of the sfx decruncher.
* Added various improvements and documentation of the desfx command.

A complete changelog is included in the zip file. (download removed)

### 2008-09-08: Exomizer v2.0 beta 7 released
Changes since the 2.0 beta 6 release:

* Fixed a bug in the 6502 forward decruncher regarding literal sequences longer than 255 bytes.
* Added z80 decruncher source code contributed by Metalbrain.
* Added experimental desfx sub-command to decrunch a sfx-generated file.
* Fix broken ROM/RAM banking NMI handling for the Atari target of the sfx sub-command.
* Added flags to customize enter and exit code for the sfx sub-command.
* Added loading of Oric tap files to the sfx sub-command.
* Oric 1 target of the sfx sub-command supports starting basic-programs.

A complete changelog is included in the zip file. (download removed)

### 2007-01-28: Exomizer v2.0 beta 6 released
Changes since the 2.0 beta 5 release:

* Bugfix: sfx -t 128 placed decrunch table at $0334, not at $0b00 where it is supposed to be.
* Bugfix: sfx -t 128 was unable to bank RAM/ROM properly.
* Bugfix: Now works when compiled in a setting where unspecified char types are unsigned.
* Added experimental Oric 1 target to the sfx command. (not properly tested, comments are welcome)

A complete changelog is included in the zip file. (download removed)

### 2006-10-08: Exomizer v2.0 beta 5 released
Changes since the 2.0 beta 4 release:

* New option -f to the mem and level sub commands that enables forward crunching.
* Added a forward decruncher contributed by Krill / Plush.
* Fixed a bug that made the sfx sub command fail on big endian architectures.
* Added a dasm syntax version of exodecrunch.s.
* Changed exodecrunch.s to support literal sequences by default.

A complete changelog is included in the zip file. (download removed)

### 2006-07-08: Exomizer v2.0 beta 4 released
Changes since the 2.0 beta 3 release:

* Change email address.
* Don't use setjmp/longjmp due to obscure bug when building using mingw with -O3 -fomit-frame-pointer.
* Add multipass functionality to the assembler in order to be able to do more complex things in the decruncher source.
* Fix the sfx decruncher to work for a wider range of i_load_addr values.
* Add optional offset,len for raw command file reading.

A complete changelog is included in the zip file. (download removed)

### 2005-11-14: Exomizer v2.0 beta 3 released
First public 2.x beta release. (Download removed)

If you're looking for builds for other operating systems, here's some external links:

* Exomizer built for AmigaOS 4 is available for download [here](http://www.os4depot.net/?function=showfile&file=emulation/utility/exomizer.lha).
* Exomizer port for MorphOS is available for download [here](http://aminet.net/dev/cross/exomizer.lha).

For older releases, see the old website [here](Home_old.md).
