# Exomizer homepage
The Exomizer is a cross-cruncher. It produces files that are intended to be used on the c64/c16/plus4 computers.

1. It is a cruncher that crunches c64/c16/+4 files.
2. It can produce stand-alone self-decompressing c64/c16/+4-files.
3. It can produce files for both "in memory" and "from disk" decrunching.
4. It handles RLE-sequences well, no packer is necessary.
5. Its stand-alone decompressor can decompress $0400-$ffff (c64) or $0400-$fcff (c16/+4)
6. It is able to crunch more than one in-file into the same out-file (link).
7. It is able to load in-files to any given address (simple relocate).

The download also contains a companion utility, Exobasic. this tool manipulates c64/plus4 basic files.

1. It can do normal basic line renumbering.
2. It can generate and prepend a machine code trampoline to a basic program that will run it.
3. It can do ugly basic line renumbering and basic line link clobbering in order to maximize compression.

These tools should compile and run on all 32-bit platforms that supports ANSI-C and console applications. The source code and precompiled binaries for DOS, Linux-i386 and Win32 are included in the download.

Any suggestions, comments and/or bug reports can be sent to [me](mailto:magli143@gmail.com), the author.

### 2004-10-03: Exomizer v1.1.5 released
This release makes the builtin decruncher handle files better that need a huge safety margin to decrunch. It also features an improved crunch algorithm that doesn't grind to a halt when it finds long stretches of equal bytes like its predecessor. On the average it should crunch a little faster and slightly better as well (very slightly). Please read the changelog.txt in the zip file for details. Download the 1.1.5 release [here](downloads/exomizer-1.1.5.zip).

### 2004-06-02: Exomizer v1.1.4p1 released
This release fixes a bug in the exodecrunch.s file. (Download removed).

### 2004-05-29: Exomizer v1.1.4 released
New in this release is a contribution section containing a 4k-demo decruncher by Ninja/The Dreams and an automatic load address calculator for memory decrunching (-l auto). (Download removed).

### 2003-08-21: Exomizer v1.1.3 released
This release contains a couple of new features compared to v1.1.2. The email address for feedback and bug reports has also changed. (Download removed).

### 2003-06-14: Exomizer v1.1.2 released
This release includes source code and examples for two stream decrunchers, a tool that simplifies crunching basic programs, some small bugfixes and a new no decrunch effect flag. It now also includes dos binaries. (Download removed).

### 2003-01-25: Exomizer v1.1.1 released
This release includes two bugfixes, a couple of new features and a one byte shorter built-in decruncher compared to v1.1. (Download removed).

### 2002-10-06: Exomizer v1.1 released
This release includes support for the c264 family (c16/plus4) and a four byte shorter built-in decruncher compared to v1.0. (Download removed).

### 2002-05-01: Exomizer v1.0 released
This is the first stable release. (Download removed).

This page was last updated 2004-10-03.
